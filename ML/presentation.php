<!-- Le nom de l'UE -->
<div class="UE">Modélisation Linéaire</div>
<div class="bcc">BCC 3 : Faire un choix de modèles et algorithmes adaptés</div>

<h3> Pré-requis </h3>
Les pré-requis pour cette UE&#xA0;:
<ul>
<li><a href="http://portail.fil.univ-lille1.fr/ls4/asd">UE ASD : Algorithmique et Structures de Donnée</a></li>
</ul>

<!-- Volume horaire et organisation de l'UE C, CTD, TD, TP -->
<h3> Organisation  </h3>

<p>
Cette unité se déroule au S6 de la licence parcours informatique. Il s'agit d'une UE obligatoire de cette mention.
</p>
<p>
Volume horaire : 1h de cours pendant 9 semaines et 1h30 alternativement de TD ou TP par semaine pendant 12 semaines.</p>

<!--<p>
Cette UE constitue un pré-requis  de l'<a href="http://portail.fil.univ-lille1.fr/ls4/asd">UE ASD : Algorithmique et Structures de Données</a>.
</p>-->



<h3> Crédits </h3>

<strong>3 ECTS</strong>


<!-- le ou les responsable(s) -->
<h3> Equipe pédagogique </h3>

<p>
<dl>
<dt>Responsable</dt>
<dd><a href="mailto:marie-emilie.voge@univ-lille.fr?subject[UE]">Marie-Emilie Voge</a></dd>
<dt>Intervenants</dt>
<dd>Omar Abdelkafi</dd>
<dd>François Lemaire</dd>
<dd>Arnaud Liefooghe</dd>
<dd>Sylvain Salvati</dd>
<dd>Léopold Weinberg</dd>
</dl>
</p>


<?php
  include 'signature.php';
?>   
