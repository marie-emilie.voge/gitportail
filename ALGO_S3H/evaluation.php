<!-- exemple de description des modalités d'évaluation d'une UE -->
<!-- la class NOTE attribue une couleur -->
<!-- la class FORMULE pour les tags <p> -->
 



L'évaluation s'effectue suivant une procédure de contrôle continu, et un examen en fin de semestre.<br>
<br>
Trois notes seront attribuées à chaque étudiant durant le semestre :<br>

<ul>
    <li> DS : une note sur 20 de Devoir Surveillé ;</li>
    <li> TD : une note sur 20 obtenue à partir de plusieurs travaux réalisés au cours du semestre (par exemple interrogations ecrites courtes, devoirs à rendre, TP ...);</li>
    <li> EX : une note sur 20 pour l'examen de fin de semestre.</li>
</ul>

<br>
La note finale sur 20 (N) est calculée de la manière suivante :<br>

<p>
N = sup(EX; (TD+DS+2EX)/4)
</p>

<br>

Pour la seconde session d'examen, les notes de TD et de DS sont conservées. Seule la note d'examen (EX) est remplacée par la note obtenue lors de la seconde session.
<br><br>
La note d'algorithmique représente 40% de la note de l'UE S3H Algo-Prog-TW pour les étudiants qui suivent le cours TW et 50% pour les autres étudiants.
<br><br>