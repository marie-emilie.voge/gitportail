<!-- objectifs de l'UE -->
<h3> Objectifs </h3>

 
<!--<p> Maîtriser les techniques algorithmiques et les structures de
données les plus communes afin d'utiliser les bons outils pour réaliser les bonnes tâches.
</p>-->

<!-- le contenu -->
<h3> Contenu </h3>

<ul>
<li>Algorithmes de parcours</li>
<li>Récursivité</li>
<li>Complexité des algorithmes</li>
<li>Tableaux à une dimension</li>
   <ul> 
   <li>recherche d'éléments</li> 
   <li>tris</li>
   </ul>
<!-- <li>Tableaux à plusieurs dimensions</li> -->
<li>Structures de donnée linéaires : listes, piles et files (suivant l'avancement du cours)</li>
<!-- <li>Arbres, arbres binaires de recherche</li> -->
</ul>

<!-- bibliographie accompagnant l'UE -->
<h3> Bibliographie </h3>

<p>
<ul>
<li>Introcution à l'algorithmique. Cormen, Leiserson, Rivest, Stein. DUNOD</li>
<li><a href="http://www-igm.univ-mlv.fr/~berstel/Elements/Elements.html">Elements d'algorithmique</a>, D. Beauquier, J. Berstel, Ph. Chrétienne</li>
</ul>
</p>


<!-- Nom de l'auteur de la page -->
<!--<div class="signature">
    
</div>
-->
