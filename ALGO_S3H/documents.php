<!-- TEMPLATE POUR documents.php -->
<h3>Exemples de sujets de DS</h3>

<ul>

<li>2017-2018 : 
<ul><li><a href="https://gitlab-ens.fil.univ-lille1.fr/voge/portail/raw/master/ALGO_S3H/CONTROLE/2017/DS1.pdf">DS1</a></li>
<li><a href="https://gitlab-ens.fil.univ-lille1.fr/voge/portail/raw/master/ALGO_S3H/CONTROLE/2017/DS2.pdf">DS2</a></li>
<li><a href="https://gitlab-ens.fil.univ-lille1.fr/voge/portail/raw/master/ALGO_S3H/CONTROLE/2017/DS3.pdf">DS3</a></li>

</ul>
</li></ul>

<!-- <h3>Documents à propos de GNUPLOT</h3>

<a href="https://gitlab-ens.fil.univ-lille1.fr/voge/portail/raw/master/ALGO_S3H/DOC/Gnuplot.html">Une synthèse de l'utilisation de Gnuplot</a>
-->

<!-- bibliographie accompagnant l'UE -->
<h3> Bibliographie </h3>

<p>
<ul>
<li>Introcution à l'algorithmique. Cormen, Leiserson, Rivest, Stein. DUNOD</li>
<li><a href="http://www-igm.univ-mlv.fr/~berstel/Elements/Elements.html">Elements d'algorithmique</a>, D. Beauquier, J. Berstel, Ph. Chrétienne</li>
</ul>
</p>


<!-- Nom de l'auteur de la page -->
<!--<div class="signature">

</div>
-->

