<div class="semainier">        


<table class="table">

<tr class="entete">
<th>  </th>
<th> Séance </th>
<th> C-TD </th>
<th> TDO </th>
<th> Remarque </th>
</tr>

</td>
</tr>
<!-- ========================================================= -->

<!-- ========================================================= -->
<!-- SEANCE 0 -->
<tr class="seance" >
<td>0</td>
<td>du 3/09 au 7/09</td>
<td> 
<!-- C-TD -->
   Séance de démarrage, rappels des structures algorithmiques élémentaires. Algorithmes de parcours itératifs.
</td>
<td>
<!-- TDO -->

</td>
<td class="remarque"> 
<!-- REMARQUE -->



</td>
</tr>
<!-- FIN SEANCE 1 -->
<!-- ========================================================= -->


<!-- ========================================================= -->
<!-- SEANCE 1 -->
<tr class="seance" >
<td>1</td>
<td>du 10/09 au 15/09</td>
<td> 
<!-- C-TD -->
Algorithmes de parcours itératifs.
</td>
<td>
<!-- TDO -->

</td>
<td class="remarque"> 
<!-- REMARQUE -->

festival MIX CITE jeudi 13 septembre après-midi

</td>
</tr>
<!-- FIN SEANCE 1 -->
<!-- ========================================================= -->

<!-- ========================================================= -->
<!-- SEANCE 2 -->
<tr class="seance" >
<td>2</td>
<td>du 17/09 au 22/09</td>
<td> 
<!-- COURS -->
   Algorithmes de parcours, nombre d'opérations, notations asymptotiques.
</td>
<td> 
<!-- C-TD -->

</td>
<td> 
<!-- TDO -->

</td>
<td class="remarque"> 
<!-- REMARQUE -->

</td>
</tr>
<!-- FIN SEANCE 2 -->
<!-- ========================================================= -->

<!-- ========================================================= -->
<!-- SEANCE 3 -->
<tr class="seance" >
<td>3</td>
<td>du 24/09 au 29/09</td>
<td> 
<!-- C-TD -->
 Algorithmes de parcours, nombre d'opérations, notations asymptotiques.
</td>
<td>
<!-- TDO -->

</td>
<td class="remarque"> 
<!-- REMARQUE -->

</td>
</tr>
<!-- FIN SEANCE 3 -->
<!-- ========================================================= -->

<!-- ========================================================= -->
<!-- SEANCE 4 -->
<tr class="seance" >
<td>4</td>
<td>du 01/10 au 06/10</td>
<td> 
<!-- C-TD -->

</td>
<td> 
<!-- TDO -->
TP sur les <a href="https://gitlab-ens.fil.univ-lille1.fr/voge/portail/raw/master/ALGO_S3H/TP/TP1/tp1.pdf">parcours</a> et <a href="https://gitlab-ens.fil.univ-lille1.fr/voge/portail/raw/master/ALGO_S3H/TP/TP1/archive_parcours.tar">archive</a> à récupérer
</td>
<td class="remarque"> 
<!-- REMARQUE -->

</td>
</tr>
<!-- FIN SEANCE 4 -->
<!-- ========================================================= -->

<!-- ========================================================= -->
<!-- SEANCE 5 -->
<tr class="seance" >
<td>5</td>
<td>du 08/10 au 13/10</td>
<td> 
<!-- C-TD -->

</td>
<td> 
<!-- TDO -->

</td>
<td class="remarque">
</td>
</tr>
<!-- FIN SEANCE 5 -->
<!-- ========================================================= -->

<!-- ========================================================= -->
<!-- SEANCE 6 -->
<tr class="seance" >
<td>6</td>
<td>du 15/10 au 20/10</td>
<td> 
<!-- C-TD -->

</td>
<td>
<!-- TDO -->

</td>
<td class="remarque"> 
<!-- REMARQUE -->

</td>
</tr>
<!-- FIN SEANCE 6 -->
<!-- ========================================================= -->


<!-- ========================================================= -->
<!-- SEANCE 7 -->
<tr class="seance">
<td>7</td>
<td>du 22/10 au  26/10</td>
<td> 
<!-- C-TD -->

</td>
<td> 
<!-- TDO -->
TP <a href="https://gitlab-ens.fil.univ-lille1.fr/voge/portail/raw/master/ALGO_S3H/TP/TP2/tp2.pdf">récursivité</a>
</td>
<td class="remarque">
<!-- REMARQUE -->
</td>
</tr>
<!-- FIN SEANCE 7 -->
<!-- ========================================================= -->

<!-- ========================================================= -->
<tr class="libre" >
<td></td>
<td>du 29/10 au 03/11</td>
<td> 
</td>
<td> 
</td>
<td class="remarque">
interruption pédagogique automne
<!-- REMARQUE -->
</td>
</tr>
<!-- ========================================================= -->


<!-- ========================================================= -->
<!-- SEANCE 8 -->
<tr class="seance" >
<td>8</td>
<td>du 5/11 au 10/11</td>
<td> 
<!-- C-TD -->
DS le 9/11/2018
</td>
<td> 
<!-- TDO -->

</td>
<td class="remarque"> 
<!-- REMARQUE -->

</td>
</tr>
<!-- FIN SEANCE 8 -->
<!-- ========================================================= -->

<!-- ========================================================= -->
<!-- SEANCE 9 -->
<tr class="seance" >
<td>9</td>
<td>du 12/11 au 17/11</td>
<td> 
<!-- C-TD -->

</td>
<td> 
<!-- TDO -->

</td>
<td class="remarque"> 
<!-- REMARQUE -->

</td>
</tr>
<!-- FIN SEANCE 9 -->
<!-- ========================================================= -->

<!-- ========================================================= -->
<!-- SEANCE 10 -->
<tr class="seance" >
<td>10</td>
<td>du 19/11 au 24/11</td>
<td> 
<!-- C-TD -->

</td>
<td> 
<!-- TDO -->
<a href="https://gitlab-ens.fil.univ-lille1.fr/voge/portail/raw/master/ALGO_S3H/TP/TP3/tp3_py.pdf">TP</a> et <a href="https://gitlab-ens.fil.univ-lille1.fr/voge/portail/raw/master/ALGO_S3H/TP/TP3/permutation.py">permutation.py</a><br>
</td>
<td class="remarque"> 
<!-- REMARQUE -->

</td>
</tr>
<!-- FIN SEANCE 10 -->
<!-- ========================================================= -->

<!-- ========================================================= -->
<!-- SEANCE 11 -->
<tr class="seance" >
<td>11</td>
<td>du 26/11 au 01/12</td>
<td> 
<!-- C-TD -->

</td>
<td> 
<!-- TDO -->

</td>
<td class="remarque"> 
<!-- REMARQUE -->

</td>
</tr>
<!-- FIN SEANCE 11 -->
<!-- ========================================================= -->

<!-- ========================================================= -->
<!-- SEANCE 12 -->
<tr class="seance" >
<td>12</td>
<td>du 03/12 au 10/12</td>
<td> 
<!-- C-TD -->

</td>
<td> 
<!-- TDO -->
<a href="https://gitlab-ens.fil.univ-lille1.fr/voge/portail/raw/master/ALGO_S3H/TP/TP4/tp4_py.pdf">TP</a> et <a href="https://gitlab-ens.fil.univ-lille1.fr/voge/portail/raw/master/ALGO_S3H/TP/TP3/fusion.py">fusion.py</a><br>
</td>
<td class="remarque"> 
<!-- REMARQUE -->

</td>
</tr>
<!-- FIN SEANCE 12 -->
<!-- ========================================================= -->


<!-- ========================================================= -->
<!-- SEANCE 13 -->
<tr class="seance" >
<td>13</td>
<td>du 11/12 au 15/12</td>
<td> 
<!-- C-TD -->

</td>
<td> 
<!-- TDO -->

</td>
<td class="remarque"> 
<!-- REMARQUE -->

</td>
</tr>
<!-- FIN SEANCE 13 -->
<!-- ========================================================= -->


<!-- ========================================================= -->
<!-- SEANCE 14 -->
<tr class="seance" >
<td>14</td>
<td>du 17/12 au 21/12</td>
<td> 
<!-- C-TD -->

</td>
<td> 
<!-- TDO -->

</td>
<td class="remarque"> 
<!-- REMARQUE -->

</td>
</tr>
<!-- FIN SEANCE 14 -->
<!-- ========================================================= -->



</table>



</div>


<?php
  include 'signature.php';
?>   
