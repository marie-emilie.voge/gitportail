from random import *
from numpy import *





# renvoie un tableau de taille n tel que la case i contient i
# indexe entre 0 et n-1
def identite(n):
    tab = arange(n)
    return tab

# echange pseudo-aleatoirement les elements du tableau t
def melange(t):
    n = t.size
    seed()
    i = 0
    while i < n:
        r = randint(i,n-1)
        tmp = t[i]
        t[i] = t[r]
        t[r] = tmp
        i = i + 1
    

# renvoie un tableau contenants une permutation des entiers de 0 a n-1
# indexe entre 0 et n-1        
def randTab(n):
    seed()
    tab = arange(n)
    i = 0
    while i < n:
        r = randint(i,n-1)
        tmp = tab[i]
        tab[i] = tab[r]
        tab[r] = tmp
        i = i + 1
    return tab


# exemples
toto = identite(10)

print(toto)

melange(toto)

print(toto)

titi = randTab(10)

print(titi)
