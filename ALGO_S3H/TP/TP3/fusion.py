from numpy import *



## effectue la fusion des parties deux parties du tableau t delimitees par les indices a, b et m
## partie 1 : entre les indices a et m
## partie 2 : entre les indices m+1 et b
## les deux parties doivent etre prealablement triees (independament l'une de l'autre)
## a la fin de la fusion le tableau t est trie entre les indices a et b

def fusionSansRecopie(t,a,b,m) :
    tmp = 0
    j = 0
    i = m+1
    while i <=b :
        j = i
        i = i+1
        while (j > a) and (t[j] < t[j-1]) :
            tmp = t[j-1]
            t[j-1] = t[j]
            t[j] = tmp
            j = j-1
