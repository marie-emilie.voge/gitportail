<!-- Le nom de l'UE -->
<h2>Algorithmique</h2>

<p>
Cet enseignement se déroule au S3 de la licence d'informatique uniquement en parcours d'harmonisation. 
</p>

<h3>Objectif</h3>

Couvrir le programme d'algorithmique des UE Informatique et AP1 de L1 et AP2 de L2S3.

<!-- le ou les responsable(s) -->
<h3>Responsable</h3>

<ul>
<li> Marie-Emilie Voge (marie-emilie.voge[AT]univ-lille.fr) </a></li>
</ul>

<!-- Volume horaire et organisation de l'UE C, CTD, TD, TP -->
<h3> Volume horaire </h3>

<ul>
<li>CTD : 3h par semaine sur 12 semaines, soit 36h au total</li>
<li>TP : 4 séances de 1h30 au cours du semestre, soit 6h au total en demi groupe</li>
</ul>

<h3> Crédits </h3>

L'enseignement d'algorithmique fait partie de l'unité spécifique au parcours S3H qui regroupe les cours de programmation et de technologies du web.

<!-- Nom de l'auteur de la page -->
<!--<div class="signature">
<br/>
</div>
-->

